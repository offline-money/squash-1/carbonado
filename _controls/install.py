

'''
https://packaging.python.org/en/latest/guides/distributing-packages-using-setuptools/#create-an-account

#
#	apt install python3.10-venv
#	pip install twine
#
'''

def add_paths_to_system (paths):
	import pathlib
	from os.path import dirname, join, normpath
	import sys
	
	this_directory = pathlib.Path (__file__).parent.resolve ()	
	for path in paths:
		sys.path.insert (0, normpath (join (this_directory, path)))

add_paths_to_system ([
	'modules_pip'
])


'''
	Caution, this removes the modules_pip, before installing the new ones.
'''

import rich
import semver
import toml

import os
import pathlib
from os.path import dirname, join, normpath
import sys

'''

'''
def install_dependencies (
	bump = "patch",
	pip_modules_path = None,
	pyproject_path = None
):
	os.system (f"rm -rf '{ pip_modules_path }'")

	#with open (pyproject_path, "rb") as f:
	#	data = tomllib.load (f)
					
	data = toml.load (pyproject_path)		
					
	dependencies = data ["project"] ["dependencies"]
	print ("dependencies:", dependencies)
	
	dependencies_list = list (map (lambda dependency : "'" + dependency + "'", dependencies))
	print (dependencies_list)
	
	script = "pip install"
	for dependency in dependencies:
		script += " '" + dependency + "'"
	
	script += f" -t '{ pip_modules_path }'"
	
	print (script)
	os.system (script)
	
	
def start ():
	this_directory = pathlib.Path (__file__).parent.resolve ()	
	base_directory = normpath (join (this_directory, ".."))	
	
	pyproject_path = normpath (join (base_directory, "pyproject.toml"))
	pip_modules_path = normpath (join (base_directory, "venues/stages_pip"))

	install_dependencies (
		pyproject_path = pyproject_path,
		pip_modules_path = pip_modules_path
	)
	
start ()