
'''
	docker run -v .:/somatic -it 2a1fe906ce18 /bin/bash
	
	#
	#	.. probably should do a fresh install here..
	#
	
	python3 /somatic/structures/modules/somatic/_status/status.proc.py
	exit
	
	chmod -R 777 ../bracelet
'''

#
#	how to publish:
#
#		python3 structures/modules/this_module/_status/status.proc.py
#		cp structures/modules/this_module/module.MD readme.md
#		(rm -rf dist && python3 -m build && twine upload dist/*)
#


def add_paths_to_system (paths):
	import pathlib
	from os.path import dirname, join, normpath
	import sys
	
	this_directory = pathlib.Path (__file__).parent.resolve ()	
	for path in paths:
		sys.path.insert (0, normpath (join (this_directory, path)))

add_paths_to_system ([
	'modules_pip'
])

def relative_path (path):
	import pathlib
	from os.path import dirname, join, normpath
	import sys

	this_directory_path = pathlib.Path (__file__).parent.resolve ()	

	return str (normpath (join (this_directory_path, path)))
	

