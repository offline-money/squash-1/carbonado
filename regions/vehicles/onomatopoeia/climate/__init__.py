
'''
	import onomatopoeia.climate as climate
	the_climate = climate.scan ();
'''


def scan ():
	return {
		"preconfigured extensions": [ 
			'.6.HTML',
			'.$.HTML',
			'.S.HTML',
			'.s.HTML', 
			'.jpg', 
			'.png' 
		]
	}