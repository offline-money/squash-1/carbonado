

def add_paths_to_system (paths):
	from os.path import dirname, join, normpath
	import pathlib
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'../../modules',
	'../../modules_pip'
])

import rich

program = '''const z = 1'''


#
#	https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/import
#
program = '''



import some_module from "some-module";


'''

import esprima
tokenized = esprima.tokenize(program)

print (tokenized)

#
#	https://stackoverflow.com/questions/76389148/esprima-for-python-does-not-parse-the-jsx-code
#
parsed_module = esprima.parseModule (program)
#print (parsed_module)
#print (type (parsed_module))
#print (dir (parsed_module))
#print (parsed_module.body)
#print (type (parsed_module.body))
print ("list:", list (parsed_module.body))

import json
print (json.dumps ({
	"list": {
		"type": ""
	}
}))

print ("".join (parsed_module.body))

#
#
#
rich.print_json (data = {
	"parsed module": list (parsed_module.body)
})
